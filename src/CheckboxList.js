import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import { getGroceryList } from './ToDoService/ToDoService'
//import Button from '@material-ui/core/Button';
import AddShoppingCartButton from '@material-ui/icons/AddShoppingCart';
import TextField from '@material-ui/core/TextField';


const styles = theme => ({
  root: {
    borderRadius: '15px',
    maxWidth: 360,
    marginTop: '5%',
    overflowX: 'auto',
    marginLeft: '40%',
    textAlign: 'center',

  },
  largeIcon: {
    width: 35,
    height: 35,
    float: 'left',
    padding: '20px',

  },
  text: {
    textDecorationLine: 'line-through',
  },
  textfield: {
    float: 'left',

  }

});

class CheckboxList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      groceryList: [],
    };
  }

  handleToggle = index => () => {

    let updatedGroceryList = this.state.groceryList;
    updatedGroceryList[index].checkbox = !updatedGroceryList[index].checkbox;

    this.setState(
      {
        groceryList: updatedGroceryList,
      }
    )
  };

  keyPress = (e) => {
    if (e.keyCode === 13) {
      console.log('value', e.target.value);
      let updatedGroceryList = this.state.groceryList;
      updatedGroceryList.push({ name: e.target.value, check: false });
      this.setState({ groceryList: updatedGroceryList })
      e.target.value = ''
      //make api call and update in 
    }
  }

  componentDidMount() {
    getGroceryList().then(result => {
      this.setState({ groceryList: result.items })
      console.log(result.items)
    }).catch((error) => {
      console.error(error)
    });
  }


  // handleChange = e => {
  //   console.log("before value",this.state.groceryList)
  //   this.setState({
  //    name: e.target.value,
  //    check:false,
  //   });
  //   console.log("after value",this.state.groceryList)
  // }

  render() {
    const { classes } = this.props;
    const { groceryList } = this.state;
    console.log('grocery list', groceryList)
    // var groceryList = this.state.groceryList;
    return (
      <Paper className={classes.root}>
        <List>
          {groceryList.map((list, i) => (
            <ListItem key={i} role={undefined} dense button>
              <Checkbox
                checked={list.checkox}
                onClick={this.handleToggle(i)}
                disableRipple
              />
              <ListItemText
                style={{
                  textDecoration: list.checkbox ? 'line-through' : 'none',
                }}
              >
                {list.item}
              </ListItemText>
            </ListItem>

          ))}
        </List>
        <AddShoppingCartButton className={classes.largeIcon} />
        < TextField
          id="List-item"
          placeholder="Add item "
          className={classes.textfield}
          onKeyDown={this.keyPress}
          margin="normal"
        />
      </Paper>

    );
  }
}

CheckboxList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CheckboxList);