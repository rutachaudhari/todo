import React from 'react';
import './App.css';
import List from './CheckboxList';
import ButtonAppBar from './ButtonAppBar';

function App() {
  return (
    <div>
      <ButtonAppBar/>
      <List/>
    </div>
  );
}

export default App;
