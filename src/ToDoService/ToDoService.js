const makeGetCall = (url) => {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(response =>
        response.json())
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      });

  });
}

const makePostCall = (url, groceryList) => {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(response =>
        response.json())
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      });

  });
}


export const getGroceryList = () => {
  return makeGetCall('http://localhost:8080/list');
}

export const addGroceryItem = (groceryList) => {
  return makePostCall('http://localhost:8080/add', groceryList)
}

